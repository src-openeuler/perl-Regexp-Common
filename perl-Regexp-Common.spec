Name:           perl-Regexp-Common
Version:        2024080801
Release:        2
Summary:        Perl Module : Regexp::Common
License:        Artistic-2.0 OR MIT OR BSD-3-Clause
URL:            https://metacpan.org/release/Regexp-Common
Source0:        https://cpan.metacpan.org/authors/id/A/AB/ABIGAIL/Regexp-Common-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:  perl-interpreter perl-generators perl(ExtUtils::MakeMaker)
BuildRequires:  perl-Test-Regexp

Obsoletes:	%{name}-tests < %{version}-%{release}
Provides:	%{name}-tests = %{version}-%{release}

%description
Regexp::Common - Provide commonly requested regular expressions

%prep
%autosetup -n Regexp-Common-%{version} -p1

%build
perl Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%files
%license LICENSE
%doc TODO README
%{perl_vendorlib}/Regexp
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2024080801-2
- drop useless perl(:MODULE_COMPAT) requirement

* Fri Sep 20 2024 xu_ping <707078654@qq.com> - 2024080801-1
- Upgrade version to 2024080801.

* Sun Mar 29 2020 Wei Xiong <myeuler@163.com> - 2017060201-1
- Package init
